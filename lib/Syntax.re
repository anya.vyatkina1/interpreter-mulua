open Base;

module Identifier = String;

module Expression = {
  module Literal = {
    [@deriving sexp_of]
    type t =
      | Nil
      | Boolean(bool)
      | Integer(int);
  };

  module BinaryOperator = {
    [@deriving sexp_of]
    type t =
      | Add
      | Subtract
      | Multiply
      | Divide
      | Equal
      | NotEqual
      | Less
      | LessOrEqual
      | Greater
      | GreaterOrEqual;
  };

  [@deriving sexp_of]
  type t =
    | Identifier(Identifier.t)
    | Literal(Literal.t)
    | BinaryExpression({
        left: t,
        operator: BinaryOperator.t,
        right: t,
      })
    | ParenthesizedExpression({
        expression: t
      })
    | InputExpression
    | UMinusExpression({
        expression: t
      })
};

module AssignmentStatement = {
  [@deriving sexp_of]
  type t = {
    left: Identifier.t,
    right: Expression.t
    };
};

  module rec BlockStatement : {
    [@deriving sexp_of]
    type t = list(Statement.t)
  } = {
    [@deriving sexp_of]
    type t = list(Statement.t)
  }
  and Statement : {
    [@deriving sexp_of]
    type t = 
    | IfElseStatement({
      condition: Expression.t,
      blockStatementIf: BlockStatement.t,
      blockStatementElse: BlockStatement.t
    })
    | IfStatement({
        condition: Expression.t,
        blockStatement: BlockStatement.t
      })
    | WhileStatement({
        condition: Expression.t,
        blockStatement: BlockStatement.t
      })
    | AssignmentStatement(AssignmentStatement.t)
    | PrintStatement({ 
      print_: Expression.t
      });
  } = {
    [@deriving sexp_of]
    type t = 
    | IfElseStatement({
      condition: Expression.t,
      blockStatementIf: BlockStatement.t,
      blockStatementElse: BlockStatement.t
    })
    | IfStatement({
        condition: Expression.t,
        blockStatement: BlockStatement.t
      })
    | WhileStatement({
        condition: Expression.t,
        blockStatement: BlockStatement.t
      })
    | AssignmentStatement(AssignmentStatement.t)
    | PrintStatement({ 
      print_: Expression.t
      });
  };


module Program = {
  [@deriving sexp_of]
  type t = BlockStatement.t
};
