let letter = [%sedlex.regexp? 'a' .. 'z' | 'A' .. 'Z'];
let digit = [%sedlex.regexp? '0' .. '9'];

let integer_literal = [%sedlex.regexp? (digit, Star(digit))]

let identifier_start = [%sedlex.regexp? letter | '_'];
let identifier_continue = [%sedlex.regexp? identifier_start | digit];
let identifier = [%sedlex.regexp?
  (identifier_start, Star(identifier_continue))
];

exception Unexpected(string);

let rec token = lexbuf => {
  open Parser_Menhir;
  open Sedlexing.Utf8;
  switch%sedlex (lexbuf) {
  | white_space => token(lexbuf)
  | "nil" => NilLiteral
  | "true" => BooleanLiteral(true)
  | "false" => BooleanLiteral(false)

  | "else" => ElseLiteral
  | "if" => IfLiteral
  | "then" => ThenLiteral
 
  | "while" => WhileLiteral
  | "do" => DoLiteral
  | "end" => EndLiteral

  | "input" => InputLiteral
  | "print" => PrintLiteral

  | "(" => LparenLiteral
  | ")" => RparenLiteral

  | integer_literal => IntegerLiteral(int_of_string(lexeme(lexbuf)))

  | "*" => MulLiteral
  | "//" => DivLiteral

  | "+" => AddLiteral
  | "-" => SubLiteral

  | "==" => EqualLiteral
  | "~=" => NotEqualLiteral
  | "<=" => LOELiteral
  | "<" => LessLiteral
  | ">=" => GOELiteral
  | ">" => GreaterLiteral
  | "=" => AssignmentLiteral

  | identifier => Identifier(lexeme(lexbuf))
 
  | eof => EOF
  | _ => raise(Unexpected(lexeme(lexbuf)))
  };
};
