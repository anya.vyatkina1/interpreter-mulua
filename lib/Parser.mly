%{
open Syntax
%}

%token EOF

%token <string> Identifier

%token NilLiteral
%token <bool> BooleanLiteral
%token <int> IntegerLiteral

%token IfLiteral ElseLiteral ThenLiteral EndLiteral WhileLiteral DoLiteral 
%token LparenLiteral RparenLiteral
%token AddLiteral SubLiteral MulLiteral DivLiteral
%token InputLiteral PrintLiteral

%token EqualLiteral
%token NotEqualLiteral
%token LessLiteral
%token LOELiteral      // <=
%token GreaterLiteral
%token GOELiteral      // >=

%token AssignmentLiteral // =

%left EqualLiteral NotEqualLiteral LessLiteral LOELiteral GreaterLiteral GOELiteral
%left AddLiteral SubLiteral
%left MulLiteral DivLiteral

%start <Program.t> program

%%

let program :=
  | ~ = blockstatement; EOF; <>

let blockstatement :=
  | ~ = list(statement); <>

let statement := 
  | ~ = assignmentstatement; <>
  | ~ = ifstatement; <>
  | ~ = ifelsestatement; <>
  | ~ = whilestatement; <>
  | ~ = printstatement; <>

let printstatement :=
  PrintLiteral; LparenLiteral; print_ = expression; RparenLiteral;
  {Statement.PrintStatement {print_}}

let assignmentstatement :=
  | left = Identifier; AssignmentLiteral; right = expression;
   {Statement.AssignmentStatement {left; right}}
 
let whilestatement :=
  |  WhileLiteral; condition = expression; DoLiteral; blockStatement = blockstatement; EndLiteral;
    {Statement.WhileStatement {condition; blockStatement}}

let ifstatement :=
   IfLiteral; condition = expression; ThenLiteral; blockStatement = blockstatement; EndLiteral;
    {Statement.IfStatement {condition; blockStatement}}
  
let ifelsestatement :=
   IfLiteral; condition = expression; ThenLiteral; blockStatementIf = blockstatement; ElseLiteral; blockStatementElse = blockstatement; EndLiteral;
    {Statement.IfElseStatement {condition; blockStatementIf; blockStatementElse}}  

let expression :=
  | ~ = Identifier;
   <Expression.Identifier>
  | ~ = literal; <>
  | ~ = parenthesizedExpression; <>
  | ~ = binaryExpression; <>
  | ~ = inputExpression; <>
  | ~ = uMinusExpression; <>

  
let uMinusExpression :=
  | SubLiteral; expression = expression; 
    {Expression.UMinusExpression 
      {expression }
    }

let binaryExpression :=
  | left = expression; operator = binaryOperator; right = expression;
    {Expression.BinaryExpression {left; operator; right}}

let binaryOperator ==
  | AddLiteral; {Expression.BinaryOperator.Add}
  | SubLiteral; {Expression.BinaryOperator.Subtract}
  | MulLiteral; {Expression.BinaryOperator.Multiply}
  | DivLiteral; {Expression.BinaryOperator.Divide}
  | EqualLiteral; {Expression.BinaryOperator.Equal}
  | NotEqualLiteral; {Expression.BinaryOperator.NotEqual}
  | LessLiteral; {Expression.BinaryOperator.Less}
  | LOELiteral; {Expression.BinaryOperator.LessOrEqual}
  | GreaterLiteral; {Expression.BinaryOperator.Greater}
  | GOELiteral; {Expression.BinaryOperator.GreaterOrEqual}

let parenthesizedExpression :=  
  | LparenLiteral; expression = expression; RparenLiteral;
    { Expression.ParenthesizedExpression {expression} }

let inputExpression :=
  | InputLiteral; LparenLiteral; RparenLiteral;
    {Expression.InputExpression}

let literal ==
  | NilLiteral;
    {
      Expression.Literal Expression.Literal.Nil
    }
  | value = BooleanLiteral;
    {
      Expression.Literal (Expression.Literal.Boolean value)
    }
  | value = IntegerLiteral;
    {
      Expression.Literal (Expression.Literal.Integer value)
    }
