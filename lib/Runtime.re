open Base;

exception WrongType
exception Divide_by_zero;

module Value = {
  [@deriving sexp_of]
  type t =
    | Nil
    | Boolean(bool)
    | Integer(int);

  let to_string =
    fun
    | Nil => "nil"
    | Boolean(x) => Bool.to_string(x)
    | Integer(x) => Int.to_string(x);

    let to_bool =
    fun
    | Nil => false
    | Boolean(x) => x
    | _ => true;
};

module Environment: {
  type t  = {var: Map.M(Syntax.Identifier).t(Value.t), ret: Value.t}; 
  let empty: t;
} = {
  type t = {var: Map.M(Syntax.Identifier).t(Value.t), ret: Value.t};
  let empty: t = { var: Map.empty((module Syntax.Identifier)), ret: Value.Nil};
};


let get_variable = (~env: Environment.t, identifier): Value.t => {
  switch(Base.Map.find(env.var, identifier)) {
    | None => Value.Nil
    | Some(x) => x;
  };
};

let set_variable = (~env: Environment.t, identifier, value): Environment.t => {
  {var: Base.Map.set(~key=identifier, ~data=value, env.var), ret: env.ret}
};

let rec eval =
    (~_env = Environment.empty, _program: Syntax.Program.t): Environment.t => 
    { 
      let  f = (_env, obj: Syntax.Statement.t) => evalStatement(~env=_env, obj)
      List.fold(_program, ~f=f, ~init = _env) 
    }


and evalStatement = (~env: Environment.t, obj: Syntax.Statement.t): Environment.t => {
  switch(obj) {
    | Syntax.Statement.WhileStatement({condition, blockStatement}) => evalWhileStatement(~env = env, ~condition = condition, ~blockStatement = blockStatement)
    | Syntax.Statement.IfStatement({condition, blockStatement}) => evalIfStatement(~env = env, ~condition = condition, ~blockStatement = blockStatement)
    | Syntax.Statement.IfElseStatement({condition, blockStatementIf, blockStatementElse}) 
      => evalIfElseStatement(~env = env, ~condition = condition, ~blockStatementIf = blockStatementIf, ~blockStatementElse = blockStatementElse)
    | Syntax.Statement.AssignmentStatement({left, right}) => evalAssignmentStatement(~env = env, ~left = left, ~right = right)
    | Syntax.Statement.PrintStatement({print_}) => evalPrintStatement(~env = env, ~print_ = print_)
    
  };
}

and evalWhileStatement = (~env: Environment.t, ~condition: Syntax.Expression.t, ~blockStatement: Syntax.BlockStatement.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = condition)
  let env = ref(env)
  while (Value.to_bool(env^.ret)) {
    env := eval(~_env = env^, blockStatement)
    env := evalExpression(~env = env^, ~obj = condition)
  }
  env^
}

and evalIfStatement = (~env: Environment.t, ~condition: Syntax.Expression.t, ~blockStatement: Syntax.BlockStatement.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = condition)
  let env_condition = env;
  let env = ref(env)
  if (Value.to_bool(env^.ret)) {
    env := eval(~_env = env^, blockStatement)
    env := env_condition;
  }
  env^  
}

and evalIfElseStatement = (~env: Environment.t, ~condition: Syntax.Expression.t, ~blockStatementIf: Syntax.BlockStatement.t, ~blockStatementElse: Syntax.BlockStatement.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = condition)
  let env_condition = env;
  let env = ref(env)
  if (Value.to_bool(env^.ret)) {
    env := eval(~_env = env^, blockStatementIf)
    env := env_condition
  } else {
    env := eval(~_env = env^, blockStatementElse)
    env := env_condition
  }
  env^  
}

and evalAssignmentStatement = (~env: Environment.t, ~left: Syntax.Identifier.t, ~right: Syntax.Expression.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = right)
  let env = ref(env)
  set_variable(~env = env^, left, env^.ret)
}

and evalPrintStatement = (~env: Environment.t, ~print_: Syntax.Expression.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = print_)
  Stdio.print_endline(Value.to_string(env.ret))
  env
}

and evalExpression = (~env: Environment.t, ~obj: Syntax.Expression.t): Environment.t => {
  switch(obj) {
    | Syntax.Expression.Literal(obj) => evalLiteral(~env = env, ~obj = obj)
    | Syntax.Expression.Identifier(obj) => evalIdentifier(~env = env, ~obj = obj)
    | Syntax.Expression.BinaryExpression({left, operator, right}) => evalBinaryExpression(~env = env, ~left = left, ~operator = operator, ~right = right)
    | Syntax.Expression.ParenthesizedExpression({expression}) => evalParenthesizedExpression(~env = env, ~expression = expression)
    | Syntax.Expression.InputExpression => evalInputExpression(~env = env)
    | Syntax.Expression.UMinusExpression({expression}) => evalUMinusExpression(~env = env, ~expression = expression)
  }
}

and evalUMinusExpression = (~env: Environment.t, ~expression: Syntax.Expression.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = expression); 
  let expression = env.ret
  let result = switch(expression){
    | Value.Integer(expression) => { Value.Integer(-expression)}
    |_ => raise(WrongType)
  };
  {...env, ret: result}
}

and evalLiteral = (~env: Environment.t, ~obj: Syntax.Expression.Literal.t): Environment.t => {
  let result = switch(obj){
    | Syntax.Expression.Literal.Nil => Value.Nil
    | Syntax.Expression.Literal.Boolean(obj) => Value.Boolean(obj)
    | Syntax.Expression.Literal.Integer(obj) => Value.Integer(obj)
  };
  {...env, ret: result}
}

and evalParenthesizedExpression = (~env: Environment.t, ~expression: Syntax.Expression.t): Environment.t => {
  evalExpression(~env = env, ~obj = expression) 
}

and evalIdentifier = (~env: Environment.t, ~obj: Syntax.Identifier.t): Environment.t => {
  {...env, ret: 
  get_variable(~env = env, obj)}
}
and evalInputExpression = (~env: Environment.t) => {
  {...env, ret:
  Value.Integer(Caml.read_int())}
}

and applyOperatorToIntegers = (~operator: Syntax.Expression.BinaryOperator.t, ~left, ~right): Value.t => {
  switch(operator) {
    | Add => Value.Integer(left + right)
    | Subtract => Value.Integer(left - right)
    | Multiply => Value.Integer(left * right)
    | Divide when right != 0 => Value.Integer(left / right)
    | Divide => raise(Divide_by_zero)
    | Equal => Value.Boolean(left == right)
    | NotEqual => Value.Boolean(left != right)
    | Less => Value.Boolean(left < right)
    | LessOrEqual => Value.Boolean(left <= right)
    | Greater => Value.Boolean(left > right)
    | GreaterOrEqual => Value.Boolean(left >= right)
  }; 
}

and applyOperatorToBooleans = (~operator: Syntax.Expression.BinaryOperator.t, ~left, ~right): Value.t => {
     switch(operator){
    | Equal => Value.Boolean(Poly.(left == right))
    | NotEqual => Value.Boolean(Poly.(left != right))
    | _ => raise(WrongType)
  }; 
}

and evalBinaryExpression = (~env: Environment.t, ~left: Syntax.Expression.t, ~operator: Syntax.Expression.BinaryOperator.t, ~right: Syntax.Expression.t): Environment.t => {
  let env = evalExpression(~env = env, ~obj = left);
  let left = env.ret
  let env = evalExpression(~env = env, ~obj = right);
  let right = env.ret
  let result = switch((left, right)){
    |(Value.Integer(left), Value.Integer(right)) => applyOperatorToIntegers(~operator = operator, ~left = left, ~right = right)
    |_ => applyOperatorToBooleans(~operator = operator, ~left = left, ~right = right)

  };
  {...env, ret: result}
}